package ru.shadowsparky.videobox.domain

import ru.shadowsparky.videolib.domain.Interactor
import ru.shadowsparky.videolib.domain.TokenRepository
import ru.shadowsparky.videolib.domain.VideosRepository
import ru.shadowsparky.videolib.domain.wrapInternetResult


class NewMoviesInteractor(
    private val repository: VideosRepository,
    private val tokenRepo: TokenRepository
) : Interactor {

    suspend fun fetchNewMovies(page: Int) = wrapInternetResult {
        repository.fetchNewVideos(page, tokenRepo.getToken().token)
    }

    suspend fun searchMovies(page: Int, search: String) = wrapInternetResult {
        repository.fetchSearchResult(search, page, tokenRepo.getToken().token)
    }
}
