package ru.shadowsparky.videobox.domain

import ru.shadowsparky.videolib.domain.Interactor
import ru.shadowsparky.videolib.domain.TokenRepository
import ru.shadowsparky.videolib.domain.VideosRepository
import ru.shadowsparky.videolib.domain.wrapInternetResult

class AboutMovieInteractor(
    private val repository: VideosRepository,
    private val tokenRepository: TokenRepository
) : Interactor {
    suspend fun fetchMovieDetails(id: Long) = wrapInternetResult {
        repository.fetchVideoDetails(id, tokenRepository.getToken().token)
    }

    suspend fun fetchMovieLinks(id: Long) = wrapInternetResult {
        repository.fetchVideoLinks(id, tokenRepository.getToken().token)
    }
}
