package ru.shadowsparky.videobox.presentation.movies.watch

import android.os.Bundle
import androidx.fragment.app.Fragment
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.presentation.Const
import ru.shadowsparky.videolib.domain.model.Episode

class WatchEpisodeFragment : Fragment(R.layout.fragment_watch_episode) {

    companion object {

        fun newInstance(episode: Episode): WatchEpisodeFragment {
            return WatchEpisodeFragment().apply {
                arguments = Bundle().apply {
                    this.putSerializable(Const.EPISODE, episode)
                }
            }
        }
    }
}
