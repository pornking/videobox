package ru.shadowsparky.videobox.presentation.movies.watch

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ru.shadowsparky.toolkit.ui.applyDividerDecor
import ru.shadowsparky.toolkit.ui.applySystemWindows
import ru.shadowsparky.toolkit.ui.binding.viewBindingNullable
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.FragmentSeasonsBinding
import ru.shadowsparky.videobox.presentation.Const
import ru.shadowsparky.videobox.presentation.movies.about.AboutMovieViewModel
import ru.shadowsparky.videobox.presentation.movies.about.AboutMovieViewModelImpl
import ru.shadowsparky.videobox.presentation.movies.about.AboutViewModelFactory
import ru.shadowsparky.videolib.Operation
import ru.shadowsparky.videolib.domain.model.Episode
import ru.shadowsparky.videolib.domain.model.VideoItem


class MovieSeasonsFragment : Fragment(R.layout.fragment_seasons) {
    private val videoItem by lazy {
        requireArguments().getSerializable(Const.VIDEO_ITEM) as VideoItem
    }
    private val factory by lazy {
        AboutViewModelFactory(requireActivity().application, videoItem)
    }
    private val viewModel: AboutMovieViewModel by lazy {
        ViewModelProvider(viewModelStore, factory)
            .get(AboutMovieViewModelImpl::class.java)
    }
    private val binding by viewBindingNullable<FragmentSeasonsBinding>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            viewModel.isLoading.observe(viewLifecycleOwner) {
                loading.isVisible = it
                contentLayout.isVisible = !it
            }
            seasonsRecyclerview.applyDividerDecor()
            seasonsRecyclerview.applySystemWindows(applyBottom = true)
            errorLayout.retryButton.setOnClickListener {
                viewModel.fetchLinks()
            }
            viewModel.videoLinks.observe(viewLifecycleOwner) {
                it ?: return@observe
                seasonsRecyclerview.isVisible = it is Operation.Success
                errorLayout.root.isVisible = it is Operation.Failure
                it.onSuccess {
                    val item = it.firstOrNull() ?: return@onSuccess
                    seasonsRecyclerview.isVisible = item.isSerial()
                    watchMovieButton.isVisible = !item.isSerial()
                    if (item.isSerial()) {
                        seasonsRecyclerview.adapter = MovieSeasonsAdapter(
                            it.firstOrNull()?.seasons ?: listOf()
                        ) {
                            WatchEpisodeDialog.newInstance(it)
                                .show(childFragmentManager, "watch-episode-fragment{$it}")
                        }
                    } else {
                        item.files?.let { files ->
                            watchMovieButton.setOnClickListener { _ ->
                                val ep = Episode(0, 0, files, null, null)
                                WatchEpisodeDialog.newInstance(ep)
                                    .show(childFragmentManager, "watch-episode-fragment{$it}")
                            }
                        }
                    }
                }
                it.onFailure { fetchError, exception ->
                    errorLayout.errorMsgTextView.text = exception.toString()
                }
            }
        }
    }

    companion object {
        fun newInstance(item: VideoItem): MovieSeasonsFragment {
            return MovieSeasonsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(Const.VIDEO_ITEM, item)
                }
            }
        }
    }
}
