package ru.shadowsparky.videobox.presentation.movies.list

import androidx.paging.ItemSnapshotList
import androidx.paging.PagingData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import ru.shadowsparky.videolib.domain.model.VideoItem

interface MoviesViewModel {
    val isLoading: StateFlow<Boolean>
    val data: Flow<PagingData<VideoItem>>

    fun saveSnapshot(snapshotList: ItemSnapshotList<VideoItem>)
    fun loadSnapshot(): PagingData<VideoItem>?
}
