package ru.shadowsparky.videobox.presentation

object Const {
    const val SEARCH_RESULT = "search_result"
    const val EPISODE = "episode"
    const val VIDEO_ITEM = "video_item"
}