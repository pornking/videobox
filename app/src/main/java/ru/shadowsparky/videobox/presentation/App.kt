package ru.shadowsparky.videobox.presentation

import ru.shadowsparky.toolkit.ui.BaseApplication
import ru.shadowsparky.videobox.BuildConfig

class App : BaseApplication() {
    override val isDebug = BuildConfig.DEBUG
}
