package ru.shadowsparky.videobox.presentation.movies.about

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.shadowsparky.videolib.domain.model.VideoItem

class AboutViewModelFactory(
    private val app: Application,
    private val videoItem: VideoItem
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == AboutMovieViewModelImpl::class.java) {
            return AboutMovieViewModelImpl(app, videoItem) as T
        }
        return modelClass.newInstance()
    }
}
