package ru.shadowsparky.videobox.presentation

import android.content.Context
import android.view.View
import android.widget.LinearLayout
import androidx.annotation.StringRes
import androidx.appcompat.view.ContextThemeWrapper
import ru.shadowsparky.toolkit.ui.binding.bindToView
import ru.shadowsparky.toolkit.ui.setHtmlText
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.InfoViewBinding

class MovieInfoView(context: Context) : LinearLayout(context) {

    constructor(context: Context, @StringRes keyCode: Int, msg: String) : this(context) {
        val themeWrapper = ContextThemeWrapper(context, R.style.Theme_Videobox)
        val view = View.inflate(themeWrapper, R.layout.info_view, this)
        val binding = bindToView(InfoViewBinding::class.java, view)
            as InfoViewBinding
        binding.apply {
            movieInfoValue.setHtmlText(msg)
            movieInfoKey.text = context.getString(keyCode) + " "
        }
    }
}
