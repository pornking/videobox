package ru.shadowsparky.videobox.presentation.movies.about

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.google.android.material.tabs.TabLayoutMediator
import ru.shadowsparky.toolkit.cache.load
import ru.shadowsparky.toolkit.navigation.back
import ru.shadowsparky.toolkit.ui.applySystemWindows
import ru.shadowsparky.toolkit.ui.binding.viewBindingNullable
import ru.shadowsparky.toolkit.ui.setShowHomeIcon
import ru.shadowsparky.toolkit.ui.setSupportActionBar
import ru.shadowsparky.toolkit.ui.setToolbarTitle
import ru.shadowsparky.toolkit.ui.transition.SlideTransition
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.FragmentAboutMovie2Binding
import ru.shadowsparky.videobox.presentation.Const
import ru.shadowsparky.videolib.domain.model.VideoItem

class AboutMovieFragment : Fragment(R.layout.fragment_about_movie2), SlideTransition {
    private val movieInfo by lazy {
        requireArguments().getSerializable(Const.VIDEO_ITEM) as VideoItem
    }
    private val binding by viewBindingNullable<FragmentAboutMovie2Binding>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            setupToolbar()
            setupPager()
        }
    }

    private fun FragmentAboutMovie2Binding.setupToolbar() {
        setSupportActionBar(toolbar)
        setShowHomeIcon()
        setHasOptionsMenu(true)
        setToolbarTitle(movieInfo.title)
        toolbar.applySystemWindows(applyTop = true)
        toolbarImage.load(movieInfo.poster, lifecycleScope)
    }

    private fun FragmentAboutMovie2Binding.setupPager() {
        viewPager.adapter = PagerAdapter(childFragmentManager, lifecycle, movieInfo)
        TabLayoutMediator(tabsLayout, viewPager) { tab, position ->
            when (position) {
                0 -> { tab.text = getString(R.string.desc) }
                1 -> { tab.text = getString(R.string.movie) }
            }
        }.attach()
    }

    override fun onOptionsItemSelected(menuItem : MenuItem) : Boolean {
        if (menuItem.itemId == android.R.id.home) { back() }
        return super.onOptionsItemSelected(menuItem)
    }

    companion object {

        fun newInstance(videoItem: VideoItem): AboutMovieFragment {
            return AboutMovieFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(Const.VIDEO_ITEM, videoItem)
                }
            }
        }
    }
}
