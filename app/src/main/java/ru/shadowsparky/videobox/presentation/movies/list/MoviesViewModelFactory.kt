package ru.shadowsparky.videobox.presentation.movies.list

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MoviesViewModelFactory(
    private val app: Application,
    private val initSearchResult: String? = null
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == MoviesViewModelImpl::class.java) {
            return MoviesViewModelImpl(app, initSearchResult) as T
        }
        error("unknown viewmodel $modelClass")
    }
}
