package ru.shadowsparky.videobox.presentation

import ru.shadowsparky.toolkit.navigation.BaseFlowFragment
import ru.shadowsparky.toolkit.navigation.getFragment
import ru.shadowsparky.videobox.presentation.movies.list.MoviesFragment

class MovieFlowFragment : BaseFlowFragment() {
    override val childFragment by lazy { getFragment<MoviesFragment>() }
}
