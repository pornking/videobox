package ru.shadowsparky.videobox.presentation.movies.watch

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.shadowsparky.toolkit.layoutInflater
import ru.shadowsparky.toolkit.recycler.BoundViewHolder
import ru.shadowsparky.toolkit.ui.binding.bindToView
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.EpisodeItemBinding
import ru.shadowsparky.videolib.domain.model.Episode

class MovieEpisodeAdapter(
    private val items: List<Episode>,
    private val listener: (Episode) -> Unit
) : RecyclerView.Adapter<BoundViewHolder<Episode>>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BoundViewHolder<Episode> {
        val inflater = parent.context.layoutInflater
        val view = inflater.inflate(R.layout.episode_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BoundViewHolder<Episode>, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(view: View) : BoundViewHolder<Episode>(view) {
        private val context = view.context
        private val binding = bindToView(EpisodeItemBinding::class.java, view) as EpisodeItemBinding

        override fun bind(item: Episode) {
            binding.apply {
                episodeInfo.text = context.getString(
                    R.string.episode,
                    item.episode.toString()
                )
                episodeLayout.setOnClickListener { _ ->
                    listener.invoke(item)
                }
            }
        }
    }
}
