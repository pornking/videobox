package ru.shadowsparky.videobox.presentation.movies.about

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import ru.shadowsparky.videobox.Inject
import ru.shadowsparky.videolib.Operation
import ru.shadowsparky.videolib.domain.FetchError
import ru.shadowsparky.videolib.domain.model.VideoDetails
import ru.shadowsparky.videolib.domain.model.VideoItem
import ru.shadowsparky.videolib.domain.model.VideoLinksResponseItem

class AboutMovieViewModelImpl(
    app: Application,
    private val videoItem: VideoItem
) : AndroidViewModel(app), AboutMovieViewModel {
    private val interactor by lazy {
        Inject.provideAboutMovieInteractor(getApplication())
    }
    override val isLoading = MutableLiveData<Boolean>()
    override val details = MutableLiveData<Operation<VideoDetails, FetchError>>()
    override val videoLinks = MutableLiveData<Operation<List<VideoLinksResponseItem>, FetchError>>()

    init {
        fetchLinks()
    }

    private fun exec(action: suspend () -> Unit) {
        viewModelScope.launch {
            isLoading.value = true
            action.invoke()
            isLoading.value = false
        }
    }

    override fun fetchMovieDetails() {
        exec {
            details.value = interactor.fetchMovieDetails(videoItem.id)
        }
    }

    override fun fetchLinks() {
        exec {
            videoLinks.value = interactor.fetchMovieLinks(videoItem.id)
        }
    }
}
