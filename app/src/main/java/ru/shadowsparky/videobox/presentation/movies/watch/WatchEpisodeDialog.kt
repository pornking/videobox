package ru.shadowsparky.videobox.presentation.movies.watch

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.presentation.Const
import ru.shadowsparky.videolib.domain.model.Episode
import ru.shadowsparky.videolib.domain.model.File

class WatchEpisodeDialog : DialogFragment() {
    private val episode by lazy {
        requireArguments().getSerializable(Const.EPISODE) as Episode
    }

    private val files: List<File> by lazy {
        episode.files.sortedBy { it.quality }
    }

    private val adapter by lazy {
        ArrayAdapter<String>(requireContext(), R.layout.dialog_select_item).apply {
            val result = files.map { getString(R.string.chose_quality, it.quality.toString()) }
            addAll(result)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.chose_quality_title)
            .setAdapter(adapter) { _, index ->
                openVideo(files[index])
            }.create()
    }

    private fun openVideo(file: File) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(file.url))
        startActivity(intent)
    }

    companion object {
        fun newInstance(episode: Episode): WatchEpisodeDialog {
            return WatchEpisodeDialog().apply {
                arguments = Bundle().apply {
                    putSerializable(Const.EPISODE, episode)
                }
            }
        }
    }
}
