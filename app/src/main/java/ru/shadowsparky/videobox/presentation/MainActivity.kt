package ru.shadowsparky.videobox.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.shadowsparky.toolkit.ui.binding.viewBindingNullable
import ru.shadowsparky.toolkit.ui.hideSystemUI
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private val binding by viewBindingNullable<ActivityMainBinding>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding?.root)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.main_fragment_layout, MovieFlowFragment())
                .setReorderingAllowed(true)
                .commit()
        }
    }
}
