package ru.shadowsparky.videobox.presentation.movies.list

import androidx.paging.PagingSource
import androidx.paging.PagingState
import ru.shadowsparky.videobox.domain.NewMoviesInteractor
import ru.shadowsparky.videolib.Operation
import ru.shadowsparky.videolib.domain.model.VideoItem

class MoviesDataSource(
    private val interactor: NewMoviesInteractor,
    private val initSearchResult: String? = null
) : PagingSource<Int, VideoItem>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, VideoItem> {
        val key = params.key ?: return LoadResult.Page(listOf(), null, null)
        val result = if (initSearchResult == null) {
            interactor.fetchNewMovies(key)
        } else {
            interactor.searchMovies(key, initSearchResult)
        }
        if (result is Operation.Failure) {
            return LoadResult.Error(result.exception)
        }
        require(result is Operation.Success)
        var prevPage: Int? = key - 1
        prevPage?.let { if (it <= 0) prevPage = null }
        val nextPage: Int? = if (result.value.has_next_page) result.value.page else null
        return LoadResult.Page(result.value.items.filter { it.year != 0 }, prevPage, nextPage)
    }

    override fun getRefreshKey(state: PagingState<Int, VideoItem>): Int? {
        return state.anchorPosition
    }
}
