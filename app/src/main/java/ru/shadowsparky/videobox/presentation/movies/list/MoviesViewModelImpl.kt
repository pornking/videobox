package ru.shadowsparky.videobox.presentation.movies.list

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ItemSnapshotList
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import ru.shadowsparky.videobox.Inject
import ru.shadowsparky.videolib.domain.model.VideoItem

class MoviesViewModelImpl(
    app: Application,
    initSearchResult: String?
) : AndroidViewModel(app), MoviesViewModel {
    override val isLoading = MutableStateFlow(false)
    private val interactor = Inject.provideNewMoviesInteractor(getApplication())
    private var snapshotList: ItemSnapshotList<VideoItem>? = null
    private val moviesDataSource = MoviesDataSource(interactor, initSearchResult)
    override var data = Pager(
        config = PagingConfig(20),
        initialKey = 0,
    ) { MoviesDataSource(interactor, initSearchResult) }.flow

    override fun loadSnapshot(): PagingData<VideoItem>? {
        val result = snapshotList ?: return null
        snapshotList = null
        return PagingData.from(result.items)
    }

    override fun saveSnapshot(snapshotList: ItemSnapshotList<VideoItem>) {
        this.snapshotList = snapshotList
    }
}
