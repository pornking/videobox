package ru.shadowsparky.videobox.presentation.movies.about

import android.os.Bundle
import android.view.View
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import ru.shadowsparky.toolkit.ui.applySystemWindows
import ru.shadowsparky.toolkit.ui.binding.viewBindingNullable
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.FragmentAboutMovieTabBinding
import ru.shadowsparky.videobox.presentation.Const
import ru.shadowsparky.videobox.presentation.MovieInfoView
import ru.shadowsparky.videolib.Operation
import ru.shadowsparky.videolib.domain.model.VideoItem

class AboutMovieFragmentTab : Fragment(R.layout.fragment_about_movie_tab) {
    private val videoItem by lazy {
        requireArguments().getSerializable(Const.VIDEO_ITEM) as VideoItem
    }
    private val binding by viewBindingNullable<FragmentAboutMovieTabBinding>()
    private val factory by lazy {
        AboutViewModelFactory(requireActivity().application, videoItem)
    }
    private val viewModel: AboutMovieViewModel by lazy {
        ViewModelProvider(viewModelStore, factory)
            .get(AboutMovieViewModelImpl::class.java)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            aboutMovieScroll.applySystemWindows(applyBottom = true)
            videoItem.obtainInfoViews().forEach { aboutMovieLayout.addView(it) }
            viewModel.isLoading.observe(viewLifecycleOwner, Observer {
                contentContainer.isVisible = !it
                progressBar.isVisible = it
            })
            aboutMovieErrorLayout.retryButton.setOnClickListener {
                viewModel.fetchMovieDetails()
            }
            viewModel.details.observe(viewLifecycleOwner) { details ->
                aboutMovieErrorLayout.root.isVisible = details is Operation.Failure
                aboutMovieLayout.isVisible = details is Operation.Success
                details.onSuccess {
                    it.status?.let {
                        getInfoView(R.string.status, it.status_text)?.let {
                            aboutMovieLayout.addView(it)
                        }
                    }
                    getInfoView(R.string.story, it.short_story)?.let {
                        aboutMovieLayout.addView(it)
                    }
                }
                details.onFailure { fetchError, exception ->
                    aboutMovieErrorLayout.errorMsgTextView.text = exception.toString()
                }
            }
        }
        viewModel.fetchMovieDetails()
    }

    private fun VideoItem.obtainInfoViews(): List<MovieInfoView> {
        return mutableListOf<MovieInfoView>().apply {
            getInfoView(R.string.actors, actors.joinToString { it.name })?.let { add(it) }
            getInfoView(R.string.genres, genres.joinToString { it.name })?.let { add(it) }
            getInfoView(R.string.country, countries.joinToString { it.name })?.let { add(it) }
            last_episode?.let {
                getInfoView(R.string.latest_ep, getString(R.string.episode_text, it.episode, it.season.toString()))?.let { add(it) }
            }
        }
    }

    private fun getInfoView(@StringRes keyCode: Int, msg: String): MovieInfoView? {
        if (msg.isEmpty()) return null
        return MovieInfoView(requireContext(), keyCode, msg)
    }

    companion object {

        fun newInstance(videoItem: VideoItem): AboutMovieFragmentTab {
            return AboutMovieFragmentTab().apply {
                arguments = Bundle().apply {
                    putSerializable(Const.VIDEO_ITEM, videoItem)
                }
            }
        }
    }
}
