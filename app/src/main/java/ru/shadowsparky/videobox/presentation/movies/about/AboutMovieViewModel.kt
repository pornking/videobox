package ru.shadowsparky.videobox.presentation.movies.about

import androidx.lifecycle.LiveData
import ru.shadowsparky.videolib.Operation
import ru.shadowsparky.videolib.domain.FetchError
import ru.shadowsparky.videolib.domain.model.VideoDetails
import ru.shadowsparky.videolib.domain.model.VideoItem
import ru.shadowsparky.videolib.domain.model.VideoLinksResponseItem

interface AboutMovieViewModel {
    val isLoading: LiveData<Boolean>
    val details: LiveData<Operation<VideoDetails, FetchError>>
    val videoLinks: LiveData<Operation<List<VideoLinksResponseItem>, FetchError>>

    fun fetchLinks()
    fun fetchMovieDetails()
}
