package ru.shadowsparky.videobox.presentation.movies.about

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import ru.shadowsparky.videobox.presentation.movies.watch.MovieSeasonsFragment
import ru.shadowsparky.videolib.domain.model.VideoItem

class PagerAdapter(
    fm: FragmentManager,
    lifecycle: Lifecycle,
    videoItem: VideoItem
) : FragmentStateAdapter(fm, lifecycle) {
    private val fragments = listOf(
        AboutMovieFragmentTab.newInstance(videoItem),
        MovieSeasonsFragment.newInstance(videoItem)
    )

    override fun getItemCount() = fragments.size

    override fun createFragment(position: Int): Fragment {
        return fragments[position]
    }
}
