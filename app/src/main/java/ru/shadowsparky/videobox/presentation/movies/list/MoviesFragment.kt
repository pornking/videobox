package ru.shadowsparky.videobox.presentation.movies.list

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.paging.LoadState
import androidx.paging.PagingData
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import ru.shadowsparky.toolkit.navigation.forward
import ru.shadowsparky.toolkit.navigation.getFragment
import ru.shadowsparky.toolkit.ui.*
import ru.shadowsparky.toolkit.ui.binding.viewBindingNullable
import ru.shadowsparky.toolkit.ui.paging.LoadingWithErrorAdapter
import ru.shadowsparky.toolkit.ui.transition.FadeTransition
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.FragmentNewMoviesBinding
import ru.shadowsparky.videobox.presentation.Const
import ru.shadowsparky.videobox.presentation.movies.about.AboutMovieFragment
import java.util.*
import kotlin.collections.ArrayList


class MoviesFragment : Fragment(R.layout.fragment_new_movies),
    SearchView.OnQueryTextListener, FadeTransition {
    private val binding by viewBindingNullable<FragmentNewMoviesBinding>()
    private val searchResult by lazy {
        arguments?.getString(Const.SEARCH_RESULT)
    }
    private val factory by lazy {
        MoviesViewModelFactory(
            requireActivity().application,
            searchResult
        )
    }
    private val viewModel: MoviesViewModel by lazy {
        ViewModelProvider(viewModelStore, factory)
            .get(MoviesViewModelImpl::class.java)
    }

    private val moviesAdapter = MoviesAdapter(lifecycleScope) {
        forward(AboutMovieFragment.newInstance(it))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.data.collect {
                moviesAdapter.submitData(it)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            setSupportActionBar(toolbar)
            searchResult?.let {
                toolbar.title = it.capitalize(Locale.ROOT)
            }
            setHasOptionsMenu(true)
            initRecycler()
            toolbar.setConnectionListener(this@MoviesFragment, toolbar.title.toString())
            errorLayout.retryButton.setOnClickListener { moviesAdapter.refresh() }
            lifecycleScope.launchWhenStarted {
                moviesAdapter.loadStateFlow.collectLatest {
                    errorLayout.root.isVisible = it.refresh is LoadState.Error && moviesAdapter.itemCount == 0
                    (it.refresh as? LoadState.Error)?.apply {
                        errorLayout.errorMsgTextView.text = this.error.toString()
                    }
                    loading.isVisible = it.refresh is LoadState.Loading && moviesAdapter.itemCount == 0
                }
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        viewModel.saveSnapshot(moviesAdapter.snapshot())
    }

    private fun FragmentNewMoviesBinding.initRecycler() {
        if (newMoviesRecycler.layoutManager == null) {
            newMoviesRecycler.applyDividerDecor()
            newMoviesRecycler.adapter = moviesAdapter
                .withLoadStateFooter(
                    LoadingWithErrorAdapter(
                        onErrorHandler = { it.toString() },
                        onRefresh = { moviesAdapter.refresh() }
                    )
                )
        }
        toolbar.applySystemWindows(applyTop = true)
        newMoviesRecycler.applySystemWindows(applyBottom = true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)
        val searchItem = menu.findItem(R.id.searchBar)
        val searchView = searchItem.actionView as SearchView
        searchView.setOnQueryTextListener(this)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return false
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (!query.isNullOrEmpty()) {
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE)
                as? InputMethodManager
            binding?.let { imm?.hideSoftInputFromWindow(it.root.windowToken, 0) }
            forward(newInstance(query))
        }
        return true
    }

    companion object {

        fun newInstance(searchResult: String?): Fragment {
            return MoviesFragment().apply {
                arguments = Bundle().apply {
                    putString(Const.SEARCH_RESULT, searchResult)
                }
            }
        }
    }
}
