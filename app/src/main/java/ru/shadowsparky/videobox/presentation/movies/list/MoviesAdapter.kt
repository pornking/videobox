package ru.shadowsparky.videobox.presentation.movies.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import ru.shadowsparky.toolkit.cache.ImageLoader
import ru.shadowsparky.toolkit.cache.load
import ru.shadowsparky.toolkit.recycler.BoundViewHolder
import ru.shadowsparky.toolkit.ui.OnClickListener
import ru.shadowsparky.toolkit.ui.binding.bindToView
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.NewMovieLayoutBinding
import ru.shadowsparky.videolib.domain.model.VideoItem

class MoviesAdapter(
    private val scope: CoroutineScope,
    private val onItemClickedListener: OnClickListener<VideoItem>
) : PagingDataAdapter<VideoItem, MoviesAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.new_movie_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class ViewHolder(view: View) : BoundViewHolder<VideoItem?>(view) {
        private val binding = bindToView(NewMovieLayoutBinding::class.java, view)
            as NewMovieLayoutBinding
        private val context = view.context

        override fun bind(item: VideoItem?) {
            item ?: return
            binding.apply {
                layout.setOnClickListener { onItemClickedListener.onClick(item) }
                val maxEpisode = item.max_episode
                maxEpisodeText.isVisible = maxEpisode != null
                if (maxEpisode != null) {
                    binding.maxEpisodeText.text = context.getString(
                        R.string.episode_text,
                        maxEpisode.episode.toString(),
                        maxEpisode.season.toString()
                    )
                }
                newMoviePoster.load(item.poster, scope)
                newMovieYear.text = item.year.toString()
                movieTitle.text = item.title
            }
        }
    }

    private companion object {
        val diffCallback = object : DiffUtil.ItemCallback<VideoItem>() {
            override fun areItemsTheSame(oldItem: VideoItem, newItem: VideoItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: VideoItem, newItem: VideoItem): Boolean {
                return oldItem == newItem
            }
        }
    }
}
