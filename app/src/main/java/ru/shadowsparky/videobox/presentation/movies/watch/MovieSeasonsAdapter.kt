package ru.shadowsparky.videobox.presentation.movies.watch

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SimpleAdapter
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import ru.shadowsparky.toolkit.recycler.BoundViewHolder
import ru.shadowsparky.toolkit.ui.OnClickListener
import ru.shadowsparky.toolkit.ui.binding.bindToView
import ru.shadowsparky.videobox.R
import ru.shadowsparky.videobox.databinding.SeasonLayoutBinding
import ru.shadowsparky.videolib.domain.model.Episode
import ru.shadowsparky.videolib.domain.model.Season

class MovieSeasonsAdapter(
    private val links: List<Season>,
    private val onClickListener: OnClickListener<Episode>
) : RecyclerView.Adapter<BoundViewHolder<Season>>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BoundViewHolder<Season> {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.season_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BoundViewHolder<Season>, position: Int) {
        holder.bind(links[position])
    }

    override fun getItemCount(): Int {
        return links.size
    }

    private inner class ViewHolder(view: View) : BoundViewHolder<Season>(view) {
        private val binding = bindToView(SeasonLayoutBinding::class.java, view) as SeasonLayoutBinding
        private val context = view.context
        private var episodesShows = false

        override fun bind(item: Season) {
            binding.apply {
                seasonRoot.setOnClickListener {
                    episodesShows = !episodesShows
                    episodesRecycler.isVisible = episodesShows
                }
                episodesCount.text = context.getString(R.string.episodes, item.episodes.entries.size.toString())
                seasonName.text = context.getString(R.string.season, item.season.toString())
                episodesRecycler.adapter = MovieEpisodeAdapter(item.episodes.entries.map { it.value }) {
                    onClickListener.onClick(it)
                }
                episodesRecycler.setHasFixedSize(true)
                episodesRecycler.isNestedScrollingEnabled = false
            }
        }
    }
}
