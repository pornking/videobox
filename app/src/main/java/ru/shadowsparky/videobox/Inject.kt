package ru.shadowsparky.videobox

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import ru.shadowsparky.Database
import ru.shadowsparky.videobox.domain.AboutMovieInteractor
import ru.shadowsparky.videobox.domain.NewMoviesInteractor
import ru.shadowsparky.videolib.data.SqliteLocalTokenRepository
import ru.shadowsparky.videolib.data.VideosRepositoryImpl
import ru.shadowsparky.videolib.domain.LocalTokenRepository
import ru.shadowsparky.videolib.domain.TokenRepository
import ru.shadowsparky.videolib.domain.VideosRepository
import ru.shadowsparky.videolib.domain.newRemoteTokenRepository

object Inject {
    private var localTokenRepository: LocalTokenRepository? = null

    @Synchronized
    private fun getLocalTokenRepo(context: Context): LocalTokenRepository {
        if (localTokenRepository == null) {
            val driver = AndroidSqliteDriver(Database.Schema, context, "data.db")
            localTokenRepository = SqliteLocalTokenRepository(driver)
        }
        return requireNotNull(localTokenRepository)
    }

    private fun provideTokenRepo(context: Context): TokenRepository {
        return newRemoteTokenRepository(getLocalTokenRepo(context))
    }

    fun provideAboutMovieInteractor(context: Context): AboutMovieInteractor {
        val repo = VideosRepositoryImpl()
        return AboutMovieInteractor(repo, provideTokenRepo(context))
    }

    fun provideNewMoviesInteractor(context: Context): NewMoviesInteractor {
        val videoRepo = VideosRepositoryImpl()
        return NewMoviesInteractor(videoRepo, provideTokenRepo(context))
    }
}
